# Introduction to Machine Learning with Python

## General Notes

This project contains a Jupyter notebook introducing some very basic concepts of machine learning and the popular Iris classification case study.

First, some simple linear algebra ideas are shown via examples with the numpy library. Then a so-called K-nearest-neighbor algorithm is trained to classify flower from the Iris dataset.

This intro is aimed at total beginners to programming and machine learning.

It was used within a 90 minute workshop at the [WHU Campus for Supply Chain Management](http://campus-for-scm.de), which targets students of business administration and young management professionals.

## Installation

This project uses popular Python libraries that can be installed via a requirements.txt file. To do so, run `pip install -r requirements.txt`. It is recommended that a virtual environment be used. To do so, run `virtualenv --python=/usr/bin/python3 venv` on a Unix-like machine (Linux and iOS).

Alternatively, it can be started in the Anaconda environment (version 4.3.0, 64-bit) running Python 3.x.

After installation, start Jupyter via running `jupyter notebook` and wait for a new tab to be opened in your default web browser. Then, open the notebook called [intro_to_machine_learning.ipynb](intro_to_machine_learning.ipynb).

## Read-only Version

For those interested in just reading the example codes without installing anything, just open this [notebook](intro_to_machine_learning.ipynb) and view the Jupyter notebook in your browser.
